
import React, { Component } from 'react';
import {
    StyleSheet,
    Text, Modal, Picker, TouchableWithoutFeedback, Keyboard,
    View, Image, TouchableOpacity, TextInput, ScrollView, Platform
} from 'react-native';
import { height, width, fontSizes } from '../utils/utils'
import CardView from 'react-native-cardview'
import DateTimePicker from 'react-native-modal-datetime-picker';
import ModalDropdown from 'react-native-modal-dropdown';
const LeavesTypes = [
    {
        label: 'Sick Leave',
        value: 'Sick Leave',
    },
    {
        label: 'Planned Leave',
        value: 'Planned Leave',
    },
    {
        label: 'Maternity Leave',
        value: 'Maternity Leave',
    },
    {
        label: 'Sick Leave',
        value: 'Sick Leave',
    },
    {
        label: 'Planned Leave',
        value: 'Planned Leave',
    },
    {
        label: 'Maternity Leave',
        value: 'Maternity Leave',
    },


];
const LeaveModal = ({ modalVisible, modalClose, TitleText, TypePlaceHolder, saveButtonClicked, datePickerVisible, startDatePickerVisibleFunction, datePickerCloseFunction, endDatePickerVisibleFunction, dateSelected, minDate, startDate, endDate, totalDays, reasonTextInputData, selectLeaveTypeClicked, leavesOptionArrayOnLeaveModal, balanceTextInputChange, balanceTextInputData, leaveTypeTextInputChange, leaveTypeTextInputData }) => {
    console.log(minDate)
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={modalClose}>

            <ScrollView keyboardShouldPersistTaps="always">
                <View style={styles.modalContainer}>
                    <TouchableOpacity onPress={modalClose} style={{ flex: 1, width: width }} />
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View style={styles.centerContainer}>
                            <Text style={[styles.textStyle, { fontSize: fontSizes.font18, color: "#000", marginBottom:TitleText == "Add Leave" ? 10 : 0 }]}>{TitleText}</Text>

                            <TouchableWithoutFeedback >
                                {TitleText == "Add Leave" ?

                                    <View style={styles.rowView}>
                                        <ModalDropdown options={leavesOptionArrayOnLeaveModal}
                                            textStyle={[styles.textStyle, { fontSize: fontSizes.font14 }]}
                                            style={{ width: "90%" }}
                                            dropdownStyle={{ width: "70%", padding: 10, }}
                                            dropdownTextStyle={{ fontSize: fontSizes.font16 }}
                                            defaultValue={TypePlaceHolder}
                                            onSelect={selectLeaveTypeClicked}
                                        >

                                        </ModalDropdown>

                                        <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />

                                    </View>
                                    :
                                    <View style={styles.rowView}>
                                        <TextInput
                                            style={[styles.textInput,{borderBottomWidth:0}]}
                                            placeholder={TypePlaceHolder}
                                            placeholderTextColor="#979797"
                                            underlineColorAndroid="transparent"
                                            onChangeText={leaveTypeTextInputChange}
                                            value={leaveTypeTextInputData}
                                        />
                                        {/* <Text style={[styles.textStyle]}>{TypePlaceHolder}</Text> */}
                                    </View>
                                }
                            </TouchableWithoutFeedback>
                            {TitleText == "Add Leave" ? null :
                                <TextInput
                                    style={[styles.textInput, { width: '35%', marginTop:10 }]}
                                    placeholder="Balance"
                                    placeholderTextColor="#979797"
                                    underlineColorAndroid="transparent"
                                    keyboardType="numeric"
                                    onChangeText={balanceTextInputChange}
                                    value={balanceTextInputData}
                                />
                            }
                            {TitleText == "Add Leave" ? null :
                                <Text style={[styles.textStyle, { marginRight: 10, fontSize: fontSizes.font10, marginBottom:-10 }]}>Validity</Text>

                            }
                            <View style={styles.dateRowView}>
                                <TouchableOpacity style={{ paddingTop: 10, flex: 2, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, marginRight: 10 }} onPress={startDatePickerVisibleFunction}>
                                    <Text style={[styles.textStyle, { marginRight: 10 }]}>{startDate == '' ? "Start Date" : startDate}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ paddingTop: 10, flex: 2, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, marginLeft: 10 }} onPress={endDatePickerVisibleFunction}>
                                    <Text style={[styles.textStyle, { marginRight: 10, }]}>{endDate == '' ? "End Date" : endDate}</Text>
                                </TouchableOpacity>
                                {TitleText == "Add Leave" ?
                                    <View style={[styles.daysView, { marginTop: 10 }]}>
                                        <Text style={styles.daysText}>{totalDays == '' ? 0 : totalDays}</Text>
                                        <Text style={[styles.daysText, { margin: 0 }]}>Days</Text>

                                    </View>
                                    : null
                                }
                            </View>
                            {TitleText == "Add Leave" ?
                                <TextInput
                                    style={styles.textInput}
                                    placeholder="Reason"
                                    placeholderTextColor="#979797"
                                    multiline={true}
                                    numberOfLines={5}
                                    underlineColorAndroid="transparent"
                                    onChangeText={reasonTextInputData}
                                    value={reasonTextInputData}
                                />
                                : null}
                            <DateTimePicker
                                isVisible={datePickerVisible}
                                onConfirm={dateSelected}
                                mode='date'
                                minimumDate={minDate}
                                onCancel={datePickerCloseFunction}
                            />




                            <TouchableOpacity onPress={saveButtonClicked}
                                style={styles.saveButton}>
                                <Text style={[styles.daysText, { color: "#2A56C6", margin: 5, fontSize: fontSizes.font14 }]}>SAVE</Text>
                            </TouchableOpacity>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableOpacity onPress={modalClose} style={{ flex: 1, width: width }} />
                </View>
            </ScrollView>

        </Modal>
    )
}
const styles = StyleSheet.create({
    modalContainer: { height: height, width: width, backgroundColor: "#00000090", alignItems: "center", justifyContent: "center" },
    centerContainer: { width: "80%", padding: 15, backgroundColor: "white", paddingRight: Platform.OS == 'ios' ? 15 : 25 },
    textStyle: { fontSize: fontSizes.font14, color: "#979797", textAlign: 'left', marginTop: 10, marginBottom: 10 },
    dateRowView: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 10 },
    rowView: { flexDirection: "row", justifyContent: "space-between", borderBottomColor: "#D8D8D8", borderBottomWidth: 1, alignItems: "center" },
    daysView: { flex: 1, marginRight: 0, marginLeft: 20, borderColor: "#D8D8D8", borderWidth: 1, alignItems: "center", justifyContent: "flex-end" },
    daysText: { color: '#E84349', fontSize: fontSizes.font12, margin: 2, },
    textInput: { borderBottomColor: "#D8D8D8", borderBottomWidth: 1, height: 40, fontSize: fontSizes.font14, marginTop: 20, color: '#979797' },
    saveButton: { alignSelf: "flex-end", marginBottom: 10, marginTop: 20 }

});






export default LeaveModal;