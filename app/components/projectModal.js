
import React, { Component } from 'react';
import {
    StyleSheet,
    Text, Modal, Picker, TouchableWithoutFeedback,Keyboard,
    View, Image, TouchableOpacity, TextInput, ScrollView, Platform
} from 'react-native';
import { height, width, fontSizes } from '../utils/utils'
import DateTimePicker from 'react-native-modal-datetime-picker';
import ModalDropdown from 'react-native-modal-dropdown';
const ProjectModal = ({ modalVisible, modalClose, TitleText, ProjectNameValue, projectNameChangeText, ProjectCodeValue, projectCodeChangeText, billingTextInputData, BillingValue, Currency, saveButtonClicked, datePickerVisible, startDatePickerVisibleFunction, datePickerCloseFunction, endDatePickerVisibleFunction, dateSelected, minDate, startDate, endDate,CurrencyOptionArrayOnProjectModal, selectCurrencyClicked}) => {
    console.log(minDate)
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={modalClose}>

            <ScrollView keyboardShouldPersistTaps="always">
                <View style={styles.modalContainer}>
                    <TouchableOpacity onPress={modalClose} style={{ flex: 1, width: width }} />
                    <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
                        <View
                            style={styles.centerContainer}
                        >

                            <Text style={[styles.textStyle, { fontSize: fontSizes.font18, color: "#000", fontWeight: "bold" }]}>{TitleText}</Text>


                            <TextInput
                                style={styles.textInput}
                                placeholder="Project Name*"
                                placeholderTextColor="#979797"
                                underlineColorAndroid="transparent"
                                onChangeText={projectNameChangeText}
                                value={ProjectNameValue}
                            />
                            <TextInput
                                style={styles.textInput}
                                placeholder="Project Code"
                                placeholderTextColor="#979797"
                                underlineColorAndroid="transparent"
                                onChangeText={projectCodeChangeText}
                                value={ProjectCodeValue}
                            />

                            <View style={styles.dateRowView}>
                                <TouchableOpacity style={{ flex: 1, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, marginRight: 20 }} onPress={startDatePickerVisibleFunction}>
                                    <Text style={[styles.textStyle, { marginRight: 10 }]}>{startDate == '' ? "Start Date" : startDate}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ flex: 1, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, marginLeft: 20 }} onPress={endDatePickerVisibleFunction}>
                                    <Text style={[styles.textStyle, { marginRight: 10, }]}>{endDate == '' ? "End Date" : endDate}</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={[styles.dateRowView]}>
                                <TextInput
                                    style={[styles.textInput, { flex: 1, marginRight: 20, marginTop: 0 }]}
                                    placeholder="Billing"
                                    placeholderTextColor="#979797"
                                    underlineColorAndroid="transparent"
                                    onChangeText={billingTextInputData}
                                    value={BillingValue}
                                    keyboardType="numeric"
                                />
                                <View style={[styles.rowView, { flex: 1, marginLeft: 20, marginTop: 0 }]}>
                                    {/* <Text style={[styles.textStyle]}>{Currency}</Text> */}
                                    <ModalDropdown options={CurrencyOptionArrayOnProjectModal}
                                            textStyle={[styles.textStyle, { fontSize: fontSizes.font14 }]}
                                            style={{ width: "80%" }}
                                            dropdownStyle={{ width: "25%", padding: 10,marginLeft:"75%" }}
                                            dropdownTextStyle={{ fontSize: fontSizes.font16 }}
                                            defaultValue={Currency}
                                            onSelect={selectCurrencyClicked}
                                        >

                                        </ModalDropdown>
                                    <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />

                                </View>
                            </View>
                            <DateTimePicker
                                isVisible={datePickerVisible}
                                onConfirm={dateSelected}
                                mode='date'
                                minimumDate={minDate}
                                onCancel={datePickerCloseFunction}
                            />




                            <TouchableOpacity onPress={saveButtonClicked}
                                style={styles.saveButton}>
                                <Text style={[styles.daysText, { color: "#2A56C6", margin: 5, fontSize: fontSizes.font14 }]}>SAVE</Text>
                            </TouchableOpacity>

                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableOpacity onPress={modalClose} style={{ flex: 1, width: width }} />
                </View>
            </ScrollView>

        </Modal>
    )
}
const styles = StyleSheet.create({
    modalContainer: { height: height, width: width, backgroundColor: "#00000090", alignItems: "center", justifyContent: "center" },
    centerContainer: { width: "80%", padding: 15, backgroundColor: "white", paddingRight: Platform.OS == 'ios' ? 15 : 25 },
    textStyle: { fontSize: fontSizes.font14, color: "#979797", textAlign: 'left', marginTop: 10, marginBottom: 10 },
    dateRowView: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 10 },
    rowView: { flexDirection: "row", justifyContent: "space-between", borderBottomColor: "#D8D8D8", borderBottomWidth: 1, alignItems: "center" },
    daysView: { flex: 1, marginRight: 0, marginLeft: 20, borderColor: "#D8D8D8", borderWidth: 1, alignItems: "center", justifyContent: "flex-end" },
    daysText: { color: '#E84349', fontSize: fontSizes.font12, margin: 2, },
    textInput: { borderBottomColor: "#D8D8D8", borderBottomWidth: 1, height: 40, marginTop: 10, color: '#979797', fontSize: fontSizes.font14 },
    saveButton: { alignSelf: "flex-end", marginBottom: 10, marginTop: 20 }

});






export default ProjectModal;