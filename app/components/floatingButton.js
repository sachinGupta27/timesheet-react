





import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View, Image, TouchableOpacity,
} from 'react-native';


const FloatingButton = ({onButtonPressed,icon }) => {
    return (
        <TouchableOpacity activeOpacity={0.5} onPress={onButtonPressed}
            style={{ position: "absolute", right: 20, bottom: 20, zIndex: 99,}}>
            <Image source={icon} />
        </TouchableOpacity>
    )
}






export default FloatingButton;