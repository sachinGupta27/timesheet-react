




/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, Image
} from 'react-native';
import { height, width, fontSizes } from '../utils/utils'

// const graphData=[
//     {}
// ]

export default class activityList extends Component {
    componentWillMount() {

    }
    render() {
        return (
            <View>
                <View style={{ flexDirection: "row", justifyContent: 'space-between',marginTop:15, transform: [{ rotate: '180deg' }] }}>

                    <View style={{ flex: 1, height: "70%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                    <View style={{ flex: 1, height: "30%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                    <View style={{ flex: 1, height: "50%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                    <View style={{ flex: 1, height: "60%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                    <View style={{ flex: 1, height: "20%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                    <View style={{ flex: 1, height: "40%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                    <View style={{ flex: 1, height: "100%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                    <View style={{ flex: 1, justifyContent: "space-between", height: width / 1.5, marginHorizontal: 5, transform: [{ rotate: '180deg' }], marginRight: 20, marginLeft: 0 }}>
                        <Text style={{ margin: 2, fontSize: 12 }}>10</Text>
                        <Text style={{ margin: 2, fontSize: 12 }}>8</Text>
                        <Text style={{ margin: 2, fontSize: 12 }}>6</Text>
                        <Text style={{ margin: 2, fontSize: 12 }}>4</Text>
                        <Text style={{ margin: 2, fontSize: 12 }}>2</Text>
                        <Text style={{ margin: 2, fontSize: 12 }}>0</Text>

                    </View>

                </View>

                <View style={{ flexDirection: "row", justifyContent: 'space-between', marginLeft: 15 }}>
                    <Text style={{ flex: 1, marginHorizontal: 5 }}>

                    </Text>
                    <Text style={{ flex: 1, marginHorizontal: 5 }}>
                        Mon
                      </Text>
                    <Text style={{ flex: 1, marginHorizontal: 5 }}>
                        Tue
                      </Text>
                    <Text style={{ flex: 1, marginHorizontal: 5 }}>
                        Wed
                      </Text>
                    <Text style={{ flex: 1, marginHorizontal: 5 }}>
                        Thu
                      </Text>
                    <Text style={{ flex: 1, marginHorizontal: 5 }}>
                        Fri
                      </Text>
                    <Text style={{ flex: 1, marginHorizontal: 5 }}>
                        Sat
                      </Text>
                    <Text style={{ flex: 1, marginHorizontal: 5 }}>
                        Sun
                      </Text>


                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});





{/* <View style={{ flexDirection: "row", justifyContent: 'space-between', transform: [{ rotate: '180deg' }] }}>

                      <View style={{ flex: 1, height: "70%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                      <View style={{ flex: 1, height: "30%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                      <View style={{ flex: 1, height: "50%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                      <View style={{ flex: 1, height: "60%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                      <View style={{ flex: 1, height: "20%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                      <View style={{ flex: 1, height: "40%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                      <View style={{ flex: 1, height: "100%", backgroundColor: "#2A56C6", marginHorizontal: 5 }}></View>
                      <View style={{ flex: 1, justifyContent: "space-between", height: width / 1.5,  marginHorizontal: 5, transform: [{ rotate: '180deg' }], marginRight: 20, marginLeft:0 }}>
                        <Text style={{ margin: 2, fontSize: 12 }}>10</Text>
                        <Text style={{ margin: 2, fontSize: 12 }}>8</Text>
                        <Text style={{ margin: 2, fontSize: 12 }}>6</Text>
                        <Text style={{ margin: 2, fontSize: 12 }}>4</Text>
                        <Text style={{ margin: 2, fontSize: 12 }}>2</Text>
                        <Text style={{ margin: 2, fontSize: 12 }}>0</Text>

                      </View>

                    </View>

                    <View style={{ flexDirection: "row", justifyContent: 'space-between',marginLeft:15 }}>
                      <Text style={{ flex: 1,  marginHorizontal: 5 }}>

                      </Text>
                      <Text style={{ flex: 1, marginHorizontal: 5 }}>
                        Mon
                      </Text>
                      <Text style={{ flex: 1,  marginHorizontal: 5 }}>
                        Tue
                      </Text>
                      <Text style={{ flex: 1,  marginHorizontal: 5 }}>
                        Wed
                      </Text>
                      <Text style={{ flex: 1,  marginHorizontal: 5 }}>
                        Thu
                      </Text>
                      <Text style={{ flex: 1,  marginHorizontal: 5 }}>
                        Fri
                      </Text>
                      <Text style={{ flex: 1,  marginHorizontal: 5 }}>
                        Sat
                      </Text>
                      <Text style={{ flex: 1,  marginHorizontal: 5 }}>
                        Sun
                      </Text>


                    </View> */}