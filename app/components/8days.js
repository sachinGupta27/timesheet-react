export function today_To_day_AfterEighth_Day() {
    var date = new Date()
    var today = date.toString();
    today = today.split(' ');
    var todayYear = today[3].slice(2, 4)
    today = today[2] + ' ' + today[1] + '`' + todayYear

    var next = date.setDate(date.getDate() + 6);
    next = new Date(next)
    next = next.toString();
    next = next.split(' ');
    var nextYear = next[3].slice(2, 4)
    next = next[2] + ' ' + next[1] + '`' + nextYear

    var dateToReturn = today + "-" + next

    return dateToReturn
}