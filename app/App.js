/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text, Dimensions,
    View, ScrollView, Image, TouchableOpacity
} from 'react-native';
import { DrawerNavigator, DrawerItems, StackNavigator } from 'react-navigation';
import AnalysisExport from './screens/analysis&export';
import Timesheet from './screens/timeSheet';
import Leaves from './screens/leaves';
import Setting from './screens/setting';
import ManageLeaveTypes from './screens/manageLeaveTypes';
import ActivityList from './screens/activityList';
import TermsCondition from './screens/T&C';
import LeaveDescription from './screens/leaveDescription';
import ProjectBreakdown from './screens/projectBreakdown';
import ProjectDevelopment from './screens/projectDevelopment';
import CreateTimesheet from './screens/createTimesheet';


import { height, width } from './utils/utils'
//const { height, width } = Dimensions.get('window')
//import Dimensions from './utils/utils'


class home extends Component {
    static navigationOptions = {
        drawerLabel: 'Logout',
        drawerIcon: ({ tintColor }) => (
            <Image source={require('./images/drawerIcons/editIcon.png')} />
        ),
    };
    render() {
        return (
            alert("kkkkk")
        );
    }
}



//export default DrawerNavigator({
const Drawer = DrawerNavigator({
    AnalysisExport: {
        screen: AnalysisExport
    },
    Timesheet: {
        screen: Timesheet
    },
    Leaves: {
        screen: Leaves
    },
    ManageProject: {
        screen: Setting
    },
    ManageLeaveTypes: {
        screen: Setting
    },
    ActivityList: {
        screen: Setting
    },
    TermsCondition: {
        screen: TermsCondition
    },
    // Logout: {
    //     screen: Logout
    // },
}, {
        contentComponent: (props) => (
            <View style={styles.container} >
                <ScrollView>
                    <View style={styles.topView}>
                        <View style={styles.userDataView}>
                            <Image source={require('./images/userImage.png')}
                                style={styles.userImage} />
                            <View style={styles.emailView}>
                                <Text style={styles.emailText}>
                                    tasacsachin@gmail.com
                                    </Text>
                                <TouchableOpacity onPress={() => alert("edit")}>
                                    <Image source={require('./images/drawerIcons/editIcon.png')} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <DrawerItems
                        {...props}
                    />
                </ScrollView>
            </View>
        ),
        drawerWidth: width - (width / 5)
    });

export default StackNavigator(
    {
        Drawer: { screen: Drawer },
        LeaveDescription: { screen: LeaveDescription },
        ProjectBreakdown: { screen: ProjectBreakdown },
        ProjectDevelopment: { screen: ProjectDevelopment },
        CreateTimesheet:{screen :CreateTimesheet}
        //Register: {screen: Register},
    },
    { headerMode: "none" }
    // {
    //     initialRouteName: "Home",
    //        headerMode: Platform.OS == "android" ? "none" : "float",
    //     header: (navigation) => ({
    //         left: (
    //             <Button
    //               title="Back"
    //               onPress={ () => navigation.goBack() }  
    //             />
    //           )
    //       })
    // }
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    topView: {
        height: 150,
        width: width - (width / 5),
        backgroundColor: "#2A56C6",
        alignItems: "center",
        justifyContent: 'center'
    },
    userDataView: {
        width: (width - (width / 5)) - 20
    },
    userImage: { height: 60, width: 60, borderRadius: 30, marginTop: 25, },
    emailText: {
        textAlign: 'left',
        color: '#fff',

    },
    emailView: { flexDirection: "row", justifyContent: "space-between", alignItems: "center", marginTop: 20, }

});
