import { Dimensions, Platform, PixelRatio } from 'react-native';


export const height = Dimensions.get('window').height;


export const width = Dimensions.get('window').width;

// export const font14 = 14
// export const font18 = 18
// export const font14 = 16
// export const font10 = 10
// export const font12 = 12

export const fontSizes = {font10:10,font12:12,font14:14,font18:18,font16:16}
