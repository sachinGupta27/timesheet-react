



/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, SafeAreaView,
    Text, ScrollView, FlatList, TextInput,
    View, Image, TouchableOpacity, TouchableWithoutFeedback
} from 'react-native';
import Header from '../components/commonHeader'
import { height, width, fontSizes } from '../utils/utils'
import TimeModal from '../components/timeModal'
export default class createTimesheet extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '', activity: '', notes: '',
            startTime: '', endTime: '', billable: false, time: "0h 0m",
            timeModalVisible: false, hourText: 0, minuteText: 0, billable: false
        }
    }
    componentWillMount() {
        alert(JSON.stringify(this.props.navigation.state.params))

        this.props.navigation.state.params.data ? this.setState({ name: this.props.navigation.state.params.data.projectName }) : null
    }

    changeText(text, placeHolder) {

        if (placeHolder == 'Project Name') {
            this.setState({ name: text })
        }
        if (placeHolder == 'Activity') {
            this.setState({ activity: text })
        }
        if (placeHolder == 'Notes') {
            this.setState({ notes: text })
        }


    }
    textInput(placeHolder, multiline, value) {
        return (
            <TextInput
                style={styles.textInput}
                placeholder={placeHolder}
                placeholderTextColor="#333333"
                multiline={multiline}
                //numberOfLines={5}
                //editable={placeHolder == 'Project Name' ? this.state.name == '' ? true : false : true}
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.changeText(text, placeHolder)}
                value={value}
            />
        )
    }
    saveClick() {
        var data = { projectName: this.state.name, activity: this.state.activity, notes: this.state.notes, billable: true, time: this.state.time }
        this.props.navigation.state.params.data.returnCallBack.callBack(data)
        this.props.navigation.goBack()
        //alert(this.state.name + this.state.activity + this.state.notes)
    }
    modalConfirmPress() {
        this.setState({ timeModalVisible: false, time: this.state.hourText + "h " + this.state.minuteText + 'm' })

    }
    hourUpBTn() {
        this.setState({ hourText: this.state.hourText + 1 })
    }
    hourDownBTn() {
        this.state.hourText >= 1 ?
            this.setState({ hourText: this.state.hourText - 1 })
            : null
    }
    minDownBTn() {
        this.state.minuteText >= 1 ?
            this.setState({ minuteText: this.state.minuteText - 1 })
            : this.setState({ minuteText: 0 })
    }
    minUpBTn() {
        this.state.minuteText <= 58 ?
            this.setState({ minuteText: this.state.minuteText + 1 })
            : this.setState({ minuteText: 0 })
    }
    render() {

        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <Header
                        HeaderLeftText="Create Timesheet"
                        headerType='back'
                        HeaderRightText='Save'
                        save={true}
                        style={{ paddingLeft: 10 }}
                        toggleDrawer={() => this.props.navigation.goBack()}
                        //headerLeftIcon={}
                        headerRightIcon={require('../images/headerIcons/tickIcon.png')}
                        headerRightIconOnPress={() => this.saveClick()}
                    />
                    <TimeModal
                        modalVisible={this.state.timeModalVisible}
                        modalClose={() => this.setState({ timeModalVisible: false })}
                        hourUpBTn={() => this.hourUpBTn()}
                        hourDownBTn={() => this.hourDownBTn()}
                        minDownBTn={() => this.minDownBTn()}
                        minUpBTn={() => this.minUpBTn()}
                        onConfirmPress={() => this.modalConfirmPress()}
                        hourText={this.state.hourText}
                        minuteText={this.state.minuteText}
                    />
                    <ScrollView keyboardShouldPersistTaps="always"
                        style={styles.mainContainer}>
                        {this.textInput("Project Name", false, this.state.name)}
                        <View style={styles.rowView}>
                            <View style={styles.clockView}>
                                <Text style={{ fontSize: fontSizes.font14, }}>
                                    12:00
                                </Text>
                                <Image source={require('../images/createTimesheet/greenClock.png')} />
                            </View>
                            <View style={styles.clockView}>
                                <Text style={{ fontSize: fontSizes.font14, }}>
                                    12:00
                                </Text>
                                <Image source={require('../images/createTimesheet/redClock.png')} />
                            </View>
                            <TouchableOpacity onPress={() => this.setState({ timeModalVisible: true })}
                                style={{ flex: 1, justifyContent: "space-between", alignItems: "center", }}>
                                <Text style={{ color: "#2A56C6", fontSize: fontSizes.font16 }}>
                                    {this.state.time}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        {this.textInput("Activity", false, this.state.activity)}
                        <View style={[styles.rowView]}>
                            <TouchableWithoutFeedback onPress={() => this.setState({ billable: !this.state.billable })}>
                                <View style={{ flexDirection: "row", flex: 1, marginHorizontal: 20 }}>
                                    <Image source={this.state.billable ? require('../images/createTimesheet/locationIcon.png') : require('../images/createTimesheet/uncheckIcon.png')} />
                                    <Text style={{ fontSize: fontSizes.font14, marginLeft: 15 }}>
                                        Billable
                                </Text>
                                </View>
                            </TouchableWithoutFeedback>
                            <View style={{ flexDirection: "row", flex: 1 }}>
                                <Image source={require('../images/createTimesheet/locationIcon.png')} />
                                <Text style={{ fontSize: fontSizes.font14, marginLeft: 15 }}>
                                    Location
                                </Text>
                            </View>
                        </View>
                        {this.textInput("Notes", true, this.state.notes)}
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#F5FCFF'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    mainContainer: { flex: 1, marginHorizontal: 15 },
    textInput: { borderBottomColor: "#D8D8D8", borderBottomWidth: 1, marginTop: 20, color: '#333333', fontSize: fontSizes.font14, paddingBottom: 10, paddingLeft: 10 },
    clockView: { flex: 1.5, justifyContent: "space-between", alignItems: "center", flexDirection: "row", marginRight: 20, paddingLeft: 10, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, paddingBottom: 10 },
    rowView: { flexDirection: "row", justifyContent: "space-between", marginTop: 25, marginBottom: 10 },
});
