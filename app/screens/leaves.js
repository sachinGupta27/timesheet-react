/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View, Image
} from 'react-native';
import { VictoryPie, VictoryChart, VictoryBar, VictoryTheme, VictoryAxis } from 'victory-native';
import { height, width, fontSizes } from '../utils/utils'
const data = [
    { quarter: 1, earnings: 13000 },
    { quarter: 2, earnings: 16500 },
    { quarter: 3, earnings: 14250 },
    { quarter: 4, earnings: 19000 }
];
var seconds = 0
var minutes = 0
var hours = 0
export default class leaves extends Component {

    constructor(props) {
        super(props)
        this.state = {
            hour: 0,
            minute: 0,
            second: 0, time: "00:00:00"
        }
    }
    static navigationOptions = {
        drawerLabel: 'Leaves',
        drawerIcon: ({ tintColor }) => (
            <Image source={require('../images/drawerIcons/leavesIcon.png')} />
        ),
    };
    myTimer() {
        seconds++;
        if (seconds >= 60) {
            seconds = 0;
            minutes++;
            if (minutes >= 60) {
                minutes = 0;
                hours++;
            }
        }

    }
    timer() {

        setInterval(() => {
            //seconds++
            console.log(Math.floor(this.state.second / 60))
            this.setState({ second: this.state.second + 1 })
        }, 1000)



    }
    componentWillMount() {
        this.timer()
    }

    render() {
        return (
            <View style={styles.container}>

                {/* <Text style={styles.welcome}>
                    {((this.state.hours > 9 ? this.state.hours : "0") + this.state.hours) + ":"+
                    ((this.state.minutes > 9 ? this.state.minutes : "0") + this.state.minutes) + ":"+
                    ((this.state.seconds > 9 ? this.state.seconds : "0") + this.state.seconds)
                    }
                </Text> */}
                <Text style={styles.welcome}>
                    {/* {Math.floor(this.state.second / 3600*60)+":"+
                        (Math.floor(this.state.second / 60*60) > 59 ? (this.state.second % 60).slice(-2) :Math.floor(this.state.second / 60) ) +
                        ':' +
                        ('0' + this.state.second % 60).slice(-2)} */}
                    {/* {('0' +this.state.second % 3600000).slice(-2)+":"+
                        ('0' +this.state.second / 60000).slice(-2)  +
                        ':' +
                        ('0' + this.state.second % 60).slice(-2)} */}
                    {(Math.floor(this.state.second / 3600) > 9 ? Math.floor(this.state.second / 3600) : "0" + Math.floor(this.state.second / 3600)) + ":" +
                        (Math.floor(this.state.second / 60) > 9 ? Math.floor(this.state.second / 60) : "0" + Math.floor(this.state.second / 60)) +
                        ':' +
                        ('0' + this.state.second % 60).slice(-2)}
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
