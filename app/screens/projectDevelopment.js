



/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, SafeAreaView,
    Text, ScrollView, FlatList,
    View, Image, TouchableOpacity
} from 'react-native';
import Header from '../components/commonHeader'
import FloatingButton from '../components/floatingButton'
import { height, width, fontSizes } from '../utils/utils'
export default class projectDevelopment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projectName: '', data: '', listArr: []
        }
    }
    componentWillMount() {
       
        var list = [
            { date: "16 May 2018", hours: 3, timeTo: "12:00", timeFrom: "15:00", location: "Location", notes: "asgfdj asdgjsa asgd jsahasgd agd asgd a gdksgd asd gks gdks gdkas gdka gd" },

            { date: "16 May 2018", hours: 3, timeTo: "12:00", timeFrom: "15:00", location: "Location", notes: "asgfdj asdgjsa asgd jsahasgd agd asgd a gdksgd asd gks gdks gdkas gdka gd" },

            { date: "16 May 2018", hours: 3, timeTo: "12:00", timeFrom: "15:00", location: "Location", notes: "asgfdj asdgjsa asgd jsahasgd agd asgd a gdksgd asd gks gdks gdkas gdka gd" },
        ]
        // setTimeout(() => {
        //     alert(JSON.stringify(this.state.data))
        // }, 500)
        this.setState({ projectName: this.props.navigation.state.params.projectName, data: this.props.navigation.state.params.data, listArr: list })
    }

    createList(item, index) {
        return (
            <View style={styles.listContainer}>
                <View style={[styles.rowView, { marginBottom: 5 }]}>
                    <View style={{ flex: 2 }}>
                        <Text style={styles.headerTextStyle}>16 May 2018</Text>
                        <Text style={[styles.headerTextStyle, { color: "#979797" }]}>12:00 - 15:00</Text>
                    </View>
                    <View style={styles.editView}>
                        <Text style={[styles.headerTextStyle, { color: "#979797", fontSize: fontSizes.font16 }]}>3h</Text>
                        <Image source={require('../images/projectDevelopmentIcons/editIcon.png')} />
                    </View>
                </View>
                <View style={{ justifyContent: "flex-start", flexDirection: "row", marginHorizontal: 15 }}>

                    <Image source={require('../images/projectDevelopmentIcons/locationIcon.png')} />
                    <Text style={[styles.headerTextStyle, { color: "#979797", marginLeft: 10 }]}>Location</Text>
                </View>
                <Text style={[styles.headerTextStyle, { margin: 10, marginHorizontal: 15 }]}>Notes</Text>
                <Text style={[styles.headerTextStyle, { marginHorizontal: 15, color: "#979797" }]}>asdgha asdasdb asdgsa gd ka gdka gdka gdka dkaskd gkag dka s asdh ashdkashdkjag skdjga kdg ka gsdkasd</Text>
            </View>
        )
    }
    onFloatingButtonPressed() {

    }
    render() {

        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <Header
                        HeaderLeftText={this.state.projectName + " | " + this.state.data.activityName}
                        headerType='back'
                        HeaderRightText=''

                        toggleDrawer={() => this.props.navigation.goBack()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIcon=''
                    //headerRightIconOnPress={} 
                    />
<FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatPlusIcon.png')} />
                    <View style={styles.mainContainer}>
                        <View style={[styles.rowView, { borderBottomColor: "#E5E5E5", borderBottomWidth: 1, marginTop: 15, }]}>
                            <Text style={[styles.headerTextStyle, { fontSize: fontSizes.font18, color: "#2A56C6" }]}>On</Text>
                            <Text style={[styles.headerTextStyle, { fontSize: fontSizes.font18, color: "#FF9800", marginRight: 15 }]}>5h</Text>
                        </View>
                        <FlatList
                            data={this.state.listArr}
                            renderItem={({ item, index }) => this.createList(item, index)}
                            keyExtractor={key => key.index}
                        />
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#F5FCFF'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    mainContainer: { flex: 1 },
    listContainer: { borderBottomColor: "#E5E5E5", borderBottomWidth: 1, marginHorizontal: 10, marginBottom: 10, paddingBottom: 10 },
    editView: { flex: 1, justifyContent: "flex-end", flexDirection: "row" },
    rowView: { flexDirection: "row", justifyContent: "space-between", paddingBottom: 5, padding: 3, margin: 15, marginTop: 0, marginBottom: 10, },
    headerTextStyle: { fontSize: fontSizes.font14, color: "#333333", textAlign: 'left', marginRight: 10, marginBottom: 10 },
    listView: { borderBottomWidth: .5, marginHorizontal: 5, marginTop: 0, paddingBottom: 5, borderBottomColor: '#E5E5E5', marginRight: 10 },
    listRowView: { flexDirection: "row", justifyContent: "space-between", padding: 5 },
    projectColumnView: { flex: 4, justifyContent: "center", },
});
