



/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text, ScrollView,
    View, Image, SafeAreaView
} from 'react-native';
import Header from '../components/commonHeader'
import { height, width, fontSizes } from '../utils/utils'
export default class leaveDescription extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }
    componentWillMount() {
        //alert(JSON.stringify(this.props.navigation.state.params.data))
        this.setState({ data: this.props.navigation.state.params.data })
        // setTimeout(() => {
        //     alert(JSON.stringify(this.state.data))
        // }, 500)
    }
    render() {

        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <Header
                        HeaderLeftText={this.state.data.date + " | " + this.state.data.totalDays + (this.state.data.totalDays > 1 ? " Days" : " Day")}
                        headerType='back'
                        //HeaderRightText=''

                        toggleDrawer={() => this.props.navigation.goBack()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIcon={require('../images/drawerIcons/editIcon.png')}
                    //headerRightIconOnPress={} 
                    />
                    <View style={styles.mainContainer}>

                        <View style={styles.rowView}>
                            <Text style={[styles.textStyle, { fontSize: fontSizes.font16 }]}>{this.state.data.type}</Text>
                            <Text style={[styles.textStyle, { marginRight: 20, color: "#06B15A", fontSize: fontSizes.font16 }]}>{this.state.data.status}</Text>
                        </View>
                        <Text style={styles.textStyle}>Reason</Text>
                        <Text style={[styles.textStyle, { color: "#979797", marginTop: 0 }]}>{this.state.data.reason}</Text>

                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#F5FCFF'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    mainContainer: { padding: 10, borderBottomColor: "#E5E5E5", borderBottomWidth: 1 },
    rowView: { flexDirection: "row", justifyContent: "space-between" },
    textStyle: { fontSize: fontSizes.font14, color: "#000", textAlign: 'left', margin: 10 },
});
