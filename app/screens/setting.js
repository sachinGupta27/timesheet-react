/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, SafeAreaView,
    Text, Keyboard,
    View, Image, ScrollView, TouchableOpacity, FlatList
} from 'react-native';

import Header from '../components/commonHeader'
import FloatingButton from '../components/floatingButton'
import ProjectModal from '../components/projectModal'

import LeaveModal from '../components/leaveModal'
import { height, width, fontSizes } from '../utils/utils'
import { dateConverter } from '../components/dateConverter'

var keyboardOpen = false
export default class analysisExport extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabId: "Projects",
            flatListdata: [],
            projectList: [],
            activitiList: [],
            leaveTypeList: [],
            projectModalVisible: false,
            projectNameModalText: '',
            projectCodeModalText: '',
            projectBillingModalText: 0,
            projectModalDatePickerVisible: false,
            datePickerType: "", minDate: '',
            startDateToShowOnMOdal: '',
            endDateToShowOnModal: '',
            startDate: '',
            endDate: '',
            projectModalTitleText: "New Project",
            projectCurrencyModalText: "Currency",
            //leave modal states

            leaveModalVisible: false,
            totalDays: 0,
            reasonTextInputData: '',
            leaveTypeOnLeaveModal: "Leave Type",
            leaveTypeListPassOnModal: [],
            leaveBalance: '',
            leaveTypeTextInputDataOnModal: ''
        }
    }
    componentWillMount() {
        var projectListArr = [
            { name: "Project XYZ", id: "AAFGT#$%^", costPerHour: '14', currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", id: "", costPerHour: '14', currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", costPerHour: '14', currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", id: "", costPerHour: '', currency: "", validityFrom: "", validityTo: "" },

            { name: "Project XYZ", id: "AAFGT#$%^", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", id: "", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", id: "", costPerHour: '', currency: "", validityFrom: "", validityTo: "" },
            { name: "Project XYZ", id: "AAFGT#$%^", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", id: "", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", costPerHour: 14, currency: "$", validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", id: "", costPerHour: '', currency: "", validityFrom: "", validityTo: "" },
        ]

        var activityListArr = [
            { name: "Metting", type: "activity" },
            { name: "Development", type: "activity" },
            { name: "Testing", type: "activity" },
            { name: "Deployment", type: "activity" },

            { name: "Metting", type: "activity" },
            { name: "Development", type: "activity" },
            { name: "Testing", type: "activity" },
            { name: "Deployment", type: "activity" },
            { name: "Metting", type: "activity" },
            { name: "Development", type: "activity" },
            { name: "Testing", type: "activity" },
            { name: "Deployment", type: "activity" },
        ]

        var leaveListArr = [
            { name: "Planned(PL)", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Sick(PL)", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Casual(CL)", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },

            { name: "Planned(PL)", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Sick(PL)", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Casual(CL)", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Planned(PL)", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Sick(PL)", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Casual(CL)", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
            { name: "Project XYZ", leaveCount: 12, validityFrom: "1 Jan `18", validityTo: "31 Dec `18" },
        ]

        var leaveTypeArr = []
        for (var i = 0; i < leaveListArr.length; i++) {

            leaveTypeArr.push(leaveListArr[i].name)
        }
        this.setState({ projectList: projectListArr, activitiList: activityListArr, leaveTypeList: leaveListArr, leaveTypeListPassOnModal: leaveTypeArr })
        setTimeout(() => {
            this.setState({ flatListdata: this.state.projectList })
        }, 300)


    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardOpen);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardClose);
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    static navigationOptions = {
        drawerLabel: 'Manage Projects',
        drawerIcon: ({ tintColor }) => (
            <Image source={require('../images/drawerIcons/manageProjectIcon.png')} />
        ),
    };
    keyboardOpen() {
        keyboardOpen = true
        //this.setState({ keyboardOpen: true })
    }

    keyboardClose() {
        keyboardOpen = false
        //this.setState({ keyboardOpen: false })
    }

    filterFunction() {

    }
    onFloatingButtonPressed() {
        if (this.state.flatListdata == this.state.projectList) {
            this.setState({
                projectModalTitleText: "New Project",
                projectNameModalText: '',
                projectCodeModalText: '',
                startDateToShowOnMOdal: '',
                endDateToShowOnModal: '',
                projectBillingModalText: '',
                projectCurrencyModalText: ''

            })
            setTimeout(() => {
                this.setState({ projectModalVisible: true })
            }, 300)
        }

        if (this.state.flatListdata == this.state.leaveTypeList) {
            this.setState({
                projectModalTitleText: "Add Leave Type",
                leaveTypeOnLeaveModal: 'Leave Type',
                leaveBalance: "",
                startDateToShowOnMOdal: '',
                endDateToShowOnModal: '',

            })
            setTimeout(() => {
                this.setState({ leaveModalVisible: true })
            }, 300)
            
        }
    }
    onTabPress(tabId) {
        this.setState({ tabId: tabId })
        if (tabId == 'Projects') {
            this.setState({ flatListdata: this.state.projectList })
        }
        if (tabId == 'Activities') {
            this.setState({ flatListdata: this.state.activitiList })
        }
        if (tabId == 'Leave Types') {
            this.setState({ flatListdata: this.state.leaveTypeList })
        }
    }
    makeTabs() {
        return (
            <View style={styles.tabView}>
                {/* first tab */}
                <TouchableOpacity onPress={() => this.onTabPress("Projects")}
                    style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.tabId == "Projects" ? 2 : 0 }]} >
                    <Text style={styles.tabText}>
                        Projects
                </Text>
                </TouchableOpacity>
                {/* second tab */}
                <TouchableOpacity onPress={() => this.onTabPress("Activities")}
                    style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.tabId == "Activities" ? 2 : 0 }]}   >
                    <Text style={styles.tabText}>
                        Activities
                </Text>
                </TouchableOpacity>
                {/* third tab */}
                <TouchableOpacity onPress={() => this.onTabPress("Leave Types")}
                    style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.tabId == "Leave Types" ? 2 : 0 }]}   >
                    <Text style={styles.tabText}>
                        Leave Types
                </Text>
                </TouchableOpacity>
            </View>
        )
    }
    createList(item, index) {

        return (
            <View key={index + "View"} style={styles.listRowView}>
                <View style={{ flex: 6, marginLeft: 10 }}>
                    <Text style={styles.projectNameText}>
                        {item.name}
                    </Text>
                    {item.id ? item.id != '' ?
                        <Text style={styles.otherText}>
                            {item.id}
                        </Text>
                        : null : null}
                    {item.leaveCount ? item.leaveCount != '' ?
                        <Text style={styles.otherText}>
                            {item.leaveCount + " Leaves"}
                        </Text>
                        : null : null}
                    {item.costPerHour ? item.costPerHour != '' ?
                        <Text style={styles.otherText}>
                            {item.costPerHour + item.currency + " per hour"}
                        </Text>
                        : null : null}
                    {item.validityFrom ? (item.validityFrom != '' && item.validityTo != '') ?
                        <Text style={styles.otherText}>
                            {"validity: " + item.validityFrom + " - " + item.validityTo}
                        </Text>
                        : null : null}
                </View>
                {item.type ? null :
                    <TouchableOpacity onPress={() => this.onEditPress(item)}
                        style={{ flex: 1, alignItems: "center" }}>
                        <Image source={require('../images/settingIcons/editIcon.png')} style={{ marginTop: 5 }} />
                    </TouchableOpacity>
                }
            </View>
        )
    }

    projectModalSaveButtonClicked() {

    }
    startDateVisibleFunction() {
        this.setState({ projectModalDatePickerVisible: true, datePickerType: 'start', minDate: '' })
    }
    endDateVisibleFunction() {
        if (this.state.startDate == '') {
            alert("Please select start date first")
        } else {
            this.setState({ projectModalDatePickerVisible: true, datePickerType: 'end' })
        }

    }
    getDateFromModal(date) {
        //alert(date)startDateToShowOnMOdal
        if (this.state.datePickerType == 'start') {
            this.setState({ startDate: date, datePickerType: '', minDate: date, projectModalDatePickerVisible: false, })
            var d = dateConverter(date)
            setTimeout(() => {
                this.setState({ startDateToShowOnMOdal: d })
            })
        } if (this.state.datePickerType == 'end') {
            this.setState({ endDate: date, datePickerType: '', projectModalDatePickerVisible: false })
            var d = dateConverter(date)
            setTimeout(() => {
                this.setState({ endDateToShowOnModal: d })
            })
        }
    }
    onEditPress(item) {

        if (this.state.flatListdata == this.state.projectList) {
            this.setState({
                projectModalTitleText: "Edit Project",
                projectNameModalText: item.name,

                startDateToShowOnMOdal: item.validityFrom,
                endDateToShowOnModal: item.validityTo,
                projectBillingModalText: item.costPerHour,
                projectCurrencyModalText: item.currency

            })
            setTimeout(() => {
                this.setState({ projectModalVisible: true })
            }, 300)
        }

        if (this.state.flatListdata == this.state.leaveTypeList) {
            this.setState({
                projectModalTitleText: "Edit Leave Type",
                leaveTypeOnLeaveModal: item.name,
                leaveBalance: ""+item.leaveCount,
                startDateToShowOnMOdal: item.validityFrom,
                endDateToShowOnModal: item.validityTo,

            })
            this.setState({ leaveModalVisible: true })
        }





    }
    closeProjectModal() {
        if (keyboardOpen) {
            Keyboard.dismiss()
        } else {
            this.setState({ projectModalVisible: false })
        }
        //
    }
    closeLeaveModal() {
        if (keyboardOpen) {
            Keyboard.dismiss()
        } else {
            this.setState({ leaveModalVisible: false })
        }
        //
    }

    onLeaveModalselectLeaveTypeClicked(index, value) {
        this.setState({ leaveTypeOnLeaveModal: value })
    }
    render() {
        return (
            <SafeAreaView
                style={styles.safeArea}>
                <View style={styles.container}>
                    <Header HeaderLeftText="Settings"
                        toggleDrawer={() => this.props.navigation.openDrawer()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIcon={require('../images/settingIcons/filterIcon.png')}
                        headerRightIconOnPress={() => this.filterFunction()} />

                    <ProjectModal
                        modalVisible={this.state.projectModalVisible}
                        modalClose={() => this.closeProjectModal()}
                        TitleText={this.state.projectModalTitleText}
                        ProjectNameValue={this.state.projectNameModalText}
                        projectNameChangeText={(text) => this.setState({ projectNameModalText: text })}
                        ProjectCodeValue={this.state.projectCodeModalText}
                        projectCodeChangeText={(text) => this.setState({ projectCodeModalText: text })}
                        BillingValue={this.state.projectBillingModalText}
                        billingTextInputData={(text) => this.setState({ projectBillingModalText: text })}
                        Currency={this.state.projectCurrencyModalText}

                        saveButtonClicked={() => this.projectModalSaveButtonClicked()}
                        datePickerVisible={this.state.projectModalDatePickerVisible}
                        startDatePickerVisibleFunction={() => this.startDateVisibleFunction()}
                        datePickerCloseFunction={() => this.setState({ projectModalDatePickerVisible: false })}
                        endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
                        dateSelected={(date) => this.getDateFromModal(date)}
                        startDate={this.state.startDateToShowOnMOdal}
                        endDate={this.state.endDateToShowOnModal}
                        minDate={new Date(this.state.startDateToShowOnMOdal)}
                        CurrencyOptionArrayOnProjectModal={["$", "1", "@", "#"]}
                        selectCurrencyClicked={(index, value) => this.setState({ projectCurrencyModalText: value })}
                    />





                    <LeaveModal
                        modalVisible={this.state.leaveModalVisible}
                        datePickerVisible={this.state.projectModalDatePickerVisible}
                        datePickerVisibleFunction={() => this.setState({ projectModalDatePickerVisible: true })}
                        startDatePickerVisibleFunction={() => this.startDateVisibleFunction()}
                        endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
                        dateSelected={(date) => this.getDateFromModal(date)}
                        startDate={this.state.startDateToShowOnMOdal}
                        endDate={this.state.endDateToShowOnModal}
                        totalDays={this.state.totalDays}
                        datePickerCloseFunction={() => this.setState({ projectModalDatePickerVisible: false })}
                        minDate={new Date(this.state.startDateToShowOnMOdal)}
                        saveButtonClicked={() => this.saveButtonClicked()}
                        reasonTextInputData={(text) => this.setState({ reasonTextInputData: text })}
                        modalClose={() => this.closeLeaveModal()}
                        TitleText="Add Leave Type" TypePlaceHolder={this.state.leaveTypeOnLeaveModal}
                        leavesOptionArrayOnLeaveModal={this.state.leaveTypeListPassOnModal}
                        selectLeaveTypeClicked={(index, value) => this.onLeaveModalselectLeaveTypeClicked(index, value)}
                        balanceTextInputData={this.state.leaveBalance}
                        balanceTextInputChange={(text) => this.setState({ leaveBalance: text })}
                        leaveTypeTextInputData={this.state.leaveTypeTextInputDataOnModal}
                        leaveTypeTextInputChange={(text) => this.setState({ leaveTypeTextInputDataOnModal: text })}
                    />
                    {this.state.flatListdata == this.state.activitiList ? null :
                        <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatPlusIcon.png')} />

                    }

                    <View style={styles.mainContainer}>
                        {this.makeTabs()}
                        <FlatList
                            data={this.state.flatListdata}
                            renderItem={({ item, index }) => this.createList(item, index)}
                            keyExtractor={key => key.index}
                            extraData={this.state}
                        />

                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    container: { flex: 1, backgroundColor: '#F5FCFF', },
    mainContainer: { flex: 1, },
    tabView: { height: 40, width: width, flexDirection: 'row', backgroundColor: "#2A56C6" },
    tabStyle: { flex: 1, alignItems: "center", justifyContent: "center" },
    weekTabView: { height: 20, width: width, flexDirection: 'row', backgroundColor: "#979797" },
    tabText: { fontSize: fontSizes.font14, color: "white", textAlign: 'center', fontWeight: "bold" },
    listRowView: { flex: 1, flexDirection: "row", padding: 10, borderBottomColor: "#E5E5E5", borderBottomWidth: 1, },
    projectNameText: { fontSize: fontSizes.font14, color: "#333333", textAlign: 'left', margin: 2 },
    otherText: { fontSize: fontSizes.font12, color: "#979797", textAlign: 'left', margin: 2 },


});
