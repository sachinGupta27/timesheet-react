/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet, SafeAreaView,
  Text, TouchableWithoutFeedback,
  View, Image, ScrollView, TouchableOpacity, FlatList
} from 'react-native';
import CardView from 'react-native-cardview'
import Header from '../components/commonHeader'
import FloatingButton from '../components/floatingButton'
import TimeLeaveTab from '../components/timesheetLeaveTab'
import BarGraph from '../components/barGraph'
import PickerDropdownModal from '../components/pickerDropdownModal'
import { height, width, fontSizes } from '../utils/utils'
import { today_To_day_AfterEighth_Day } from '../components/8days'
import DateTimePicker from 'react-native-modal-datetime-picker';
const billableData = ["1", '2', '3', '4', '5', '6']
export default class analysisExport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      TabPress: true,
      showSummary: false,
      projectList: [],
      leaveList: [],
      showProjects: false,
      activityList: [],
      showActivity: false,
      isDateTimePickerVisible: false,
      pickerDropdownModalVisible: false,
      pickerDropdownModalTitleText: "Select Project",
      type: '',
      selectProject: "All Project",
      billableAmount: "Billable",
      leaveType: "All",
      showBarGraph: false

    }
  }
  static navigationOptions = {
    drawerLabel: 'Analysis & Export',
    drawerIcon: ({ tintColor }) => (
      <Image source={require('../images/drawerIcons/analysisIcon.png')} />
    ),
  };
  componentWillMount() {
    var projectArr = [
      { "projectName": "ProjectXYZ asd asdas ", 'price': 2, 'progress': 80, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'price': 5, 'progress': 40, 'time': "7h 20m" },
      { "projectName": "ProjectXYZ", 'price': 6, 'progress': 60, 'time': "4h 20m" },
      { "projectName": "ProjectXYZ", 'price': 2, 'progress': 10, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'price': 2, 'progress': 45, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'price': 2, 'progress': 80, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'price': 5, 'progress': 40, 'time': "7h 20m" },
      { "projectName": "ProjectXYZ", 'price': 6, 'progress': 60, 'time': "4h 20m" },
      { "projectName": "ProjectXYZ", 'price': 2, 'progress': 10, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'price': 2, 'progress': 45, 'time': "10h 20m" },

    ]

    var leaveArr = [
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Planned Leave", 'progress': 80, 'totalDays': 4 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 1 },
      { 'type': "Sick Leave", 'progress': 80, 'totalDays': 5 },

    ]

    var activityArr = [
      { 'activity': "Activity 1", 'time': "10h 20m" },
      { 'activity': "Activity 2", 'time': "10h 20m" },
      { 'activity': "Activity 3", 'time': "10h 20m" },
      { 'activity': "Activity 4", 'time': "10h 20m" },
      { 'activity': "Activity 5", 'time': "10h 20m" },
      { 'activity': "Activity 6", 'time': "10h 20m" },
      { 'activity': "Activity 3", 'time': "10h 20m" },
      { 'activity': "Activity 4", 'time': "10h 20m" },
      { 'activity': "Activity 5", 'time': "10h 20m" },
      { 'activity': "Activity 6", 'time': "10h 20m" },

    ]
    var days = 0
    for (var i = 0; i < leaveArr.length; i++) {
      days = days + leaveArr[i].totalDays
    }
    this.setState({ projectList: projectArr, leaveList: leaveArr, totalLeaveDays: days, activityList: activityArr })
  }
  onTabPress() {
    this.setState({ TabPress: !this.state.TabPress })
  }
  createList(item, index) {
    var progress = item.progress + "%"
    var left = (100 - item.progress) + "%"
    return (
      <TouchableWithoutFeedback key={index + "row"} >
        <View style={styles.listView}>
          <View style={[styles.rowView, { padding: this.state.TabPress ? 5 : 10 }]}>
            {this.state.TabPress ?
              <View style={{ justifyContent: 'center', alignItems: "center", marginHorizontal: 5, marginRight: 10 }}>
                <View style={{ height: 16, width: 16, borderRadius: 8, backgroundColor: "red", marginTop: -12, marginLeft: -5 }}>
                </View>
              </View>
              : null}
            <View style={[styles.projectColumnView, { flex: this.state.TabPress ? 3.5 : 1.5 }]}>
              <Text style={{ fontSize: fontSizes.font14, color: "#333333" }}>
                {this.state.TabPress ? item.projectName : item.type}
              </Text>
              {this.state.TabPress ?
                <Text style={{ fontSize: fontSizes.font12, color: "#BBBBBB", paddingTop: 5 }}>
                  {"$" + item.price}
                </Text>
                : null}

            </View>
            {!this.state.TabPress ? <View style={[styles.projectColumnView, { flex: 1.5, marginHorizontal: 5, flexDirection: "row", alignItems: "center" }]}>
              <View style={{ width: progress, backgroundColor: "#2A56C6", height: 2 }} />
              <View style={{ width: left, backgroundColor: "#D8D8D8", height: 2 }} />
            </View>
              : null}
            <View style={[styles.projectColumnView, { flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }]}>

              <Text style={{ fontSize: fontSizes.font16, color: "#6C6C6C", marginRight: 10 }}>
                {this.state.TabPress ? item.time : item.totalDays > 1 ? item.totalDays + " Days" : item.totalDays + " Day"}
              </Text>
              {!this.state.TabPress ?
                <Image source={require('../images/rightArrow.png')} /> : null
              }


            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  createActivityList(item, index) {
    return (
      <TouchableWithoutFeedback key={index + "row"} >
        <View style={[styles.listView, { marginHorizontal: 0 }]}>
          <View style={[styles.rowView, { padding: this.state.TabPress ? 5 : 10 }]}>
            <View style={[styles.projectColumnView, { flex: this.state.TabPress ? 3.5 : 1.5 }]}>
              <Text style={{ fontSize: fontSizes.font16, color: "#333333", marginLeft: 15 }}>
                {item.activity}
              </Text>
            </View>

            <View style={[styles.projectColumnView, { flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }]}>
              <Text style={{ fontSize: fontSizes.font16, color: "#6C6C6C", marginRight: 10 }}>
                {item.time}
              </Text>
            </View>
          </View>
        </View>

      </TouchableWithoutFeedback>
    )
  }
  createFlatList(listType) {

    return (
      <FlatList
        data={listType == 'activities' ? this.state.activityList : this.state.TabPress ? this.state.projectList : this.state.leaveList}
        renderItem={({ item, index }) => listType == 'activities' ? this.createActivityList(item, index) : this.createList(item, index)}
        keyExtractor={key => key.index}
      />
    )
  }
  showDatePicker() {
    this.setState({ isDateTimePickerVisible: true })
  }
  _handleDatePicked(date) {
    this.setState({ isDateTimePickerVisible: false })
    //alert(date)
  }
  onFloatingButtonPressed() {
    alert()
  }
  onProjectSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, pickerDropdownModalTitleText: "Select Project", PickerDropdownModalFlatListData: this.state.projectList, type: "Select Project" })
  }
  onBillableSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, pickerDropdownModalTitleText: 'Select Billable', PickerDropdownModalFlatListData: billableData, type: "Billable" })
  }
  onLeaveSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, pickerDropdownModalTitleText: 'Leave Type', PickerDropdownModalFlatListData: this.state.leaveList, type: "Leave Type" })
  }
  pickerDropdownModalRowSelected(item) {
    if (this.state.type == 'Select Project') {
      this.setState({ selectProject: item.projectName })
    } else if (this.state.type == 'Leave Type') {
      this.setState({ leaveType: item.type })
    }
    else {
      this.setState({ billableAmount: item })
    }

    this.setState({ pickerDropdownModalVisible: false })
  }
  render() {
    return (
      <SafeAreaView
        style={styles.safeArea}>
        <View style={styles.container}>
          <Header HeaderLeftText="Analysis"
            HeaderRightText={today_To_day_AfterEighth_Day()}
            type="date"
            toggleDrawer={() => this.props.navigation.openDrawer()}
            //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
            headerRightIcon={require('../images/analysisExportIcons/dateIcon.png')}
            headerRightIconOnPress={() => this.showDatePicker()} />
          <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatDownloadIcon.png')} />


          <PickerDropdownModal
            pickerDropdownModalVisible={this.state.pickerDropdownModalVisible}
            pickerDropdownModalClose={() => this.setState({ pickerDropdownModalVisible: false })}
            titleText={this.state.pickerDropdownModalTitleText}
            pickerDropdownModalListData={this.state.PickerDropdownModalFlatListData}
            pickerDropdownModalRow={({ item, index }) =>
              <TouchableWithoutFeedback onPress={() => this.pickerDropdownModalRowSelected(item)}>
                <View style={{ padding: 15, paddingLeft: 25, alignItems: "flex-start", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1 }}>
                  <Text style={{ fontSize: fontSizes.font18, color: "#333333" }}>
                    {item.projectName ? item.projectName : item.type ? item.type : item}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            }
          />

          {/* <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={(date)=>this._handleDatePicked(date)}
          mode='date'
          onCancel={()=>this.setState({ isDateTimePickerVisible: false })}
        /> */}
          <View style={styles.mainContainer}>
            {/* {this.timesheet_LeavesTab()} */}
            <TimeLeaveTab onTabPress={() => this.onTabPress()} tabPress={this.state.TabPress} />

            <ScrollView keyboardShouldPersistTaps="always">
              <View style={[styles.projectListContainer, { marginLeft: 0, marginRight: 0 }]}>
                {this.state.TabPress ?
                  <View style={[styles.dropdownRowView]}>
                    <TouchableOpacity onPress={() => this.onProjectSelectDropdown()}
                      style={styles.leftDropdownStyle}>
                      <Text style={[styles.ProjectHeaderText, { color: 'black' }]}>
                        {this.state.selectProject}
                      </Text>
                      <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onBillableSelectDropdown()}
                      style={[styles.rightDropdownStyle, { marginRight: 0 }]}>
                      <Text style={[styles.ProjectHeaderText, { color: 'black' }]}>
                        {this.state.billableAmount}
                      </Text>
                      <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />
                    </TouchableOpacity>
                  </View>
                  :
                  <View style={styles.dropdownRowView}>
                    <View style={{ flex: 1 }} />
                    <TouchableOpacity onPress={() => this.onLeaveSelectDropdown()}
                      style={[styles.rightDropdownStyle, { justifyContent: 'flex-end', marginRight: 0 }]}>
                      <Text style={[styles.ProjectHeaderText, { color: 'black', textAlign: "right", marginRight: 10 }]}>
                        {this.state.leaveType}
                      </Text>
                      <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />
                    </TouchableOpacity>
                  </View>
                }

                {/* Summary Card Code */}
                {/* <CardView
                style={{ backgroundColor: 'transparent', marginTop: 10, paddingBottom: 10, paddingTop: 5 }}
                cardElevation={2}

                cardMaxElevation={2}
                cornerRadius={2}> */}

                <View style={[styles.summaryContainer, { paddingLeft: 0, paddingRight: 0 }]}>
                  <TouchableOpacity onPress={() => this.setState({ showSummary: !this.state.showSummary })}
                    style={[styles.summaryColumnView, { borderBottomWidth: this.state.showSummary ? 1 : 0, paddingLeft: 0 }]}>
                    <View style={styles.summaryRowView}>
                      <Image style={{ marginTop: 4 }} source={require('../images/analysisExportIcons/downArrow.png')} />
                      <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left", marginLeft: 10 }]}>
                        Summary (Totals)
                      </Text>

                    </View>
                    {!this.state.TabPress ?
                      <Text style={styles.totalnotChangeText}>
                        Total will not change with date filter
                    </Text> : null
                    }

                  </TouchableOpacity>
                  {this.state.showSummary ?
                    <View style={[styles.rowView, { padding: 10 }]}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View
                          style={{ flex: 1, alignItems: "center" }}>

                          <Text style={[styles.ProjectHeaderText, { fontSize: 20, marginTop: 5 }]}>
                            40h 40m
                        </Text>
                          <Text style={{ color: "#333333", textAlign: "center" }}>
                            {this.state.TabPress ? 'Time' : 'Accrued'}
                          </Text>
                        </View>
                        <View
                          style={{ flex: 1, alignItems: "center" }}>

                          <Text style={[styles.ProjectHeaderText, { fontSize: 20, marginTop: 5 }]}>
                            750
                        </Text>
                          <Text style={{ color: "#333333", textAlign: "center", }}>
                            {this.state.TabPress ? "Billing ($)" : "Availed"}
                          </Text>
                        </View>
                        <View
                          style={{ flex: 1, alignItems: "center" }}>

                          <Text style={[styles.ProjectHeaderText, { fontSize: 20, marginTop: 5 }]}>
                            8
                        </Text>
                          <Text style={{ color: "#333333", textAlign: "center", }}>
                            {this.state.TabPress ? "Daily Avg. Hrs" : "Balance"}
                          </Text>
                        </View>
                      </View>
                    </View>
                    : null}
                </View>

                {/* </CardView> */}

                {/* Project Card Code */}
                <View
                  style={{ backgroundColor: 'transparent', marginTop: 10, paddingBottom: 10, borderWidth: 0, borderColor: "#E5E5E588", borderRadius: 6 }}
                >
                  <View style={[styles.headerView, { borderBottomWidth: this.state.showProjects || !this.state.TabPress ? 1 : 0, marginHorizontal: 5 }]}>
                    <TouchableWithoutFeedback onPress={() => this.setState({ showProjects: !this.state.showProjects })}>
                      <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 10, paddingBottom: 10 }}>
                        <View style={[styles.summaryRowView, { flex: 1 }]}>
                          {this.state.TabPress ?
                            <View >
                              <Image style={{ marginTop: 4 }} source={require('../images/analysisExportIcons/downArrow.png')} />
                            </View>
                            : null}
                          <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left", marginLeft: 10 }]}>
                            {this.state.TabPress ? "Projects" : "Type"}
                          </Text>

                        </View>
                        {/* <Text style={styles.ProjectHeaderText}>
                      {this.state.TabPress ? "Projects" : "Type"}
                    </Text> */}
                        <Text style={[styles.ProjectHeaderTime, { marginRight: 15 }]}>

                          {this.state.TabPress ? "40h 40m" : this.state.totalLeaveDays > 1 ? this.state.totalLeaveDays + " Days" : this.state.totalLeaveDays + " Day"}

                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  </View>
                  {this.state.TabPress ? this.state.showProjects ? this.createFlatList("projectLeave") : null : this.createFlatList("projectLeave")}
                </View>
                {/* Activities Card Code */}

                {this.state.TabPress ?
                  <View
                    style={{ backgroundColor: 'transparent', marginTop: 10, paddingBottom: 10, paddingRight: 15, borderWidth: 0, borderColor: "#E5E5E588", borderRadius: 6 }}
                  >
                    <View style={[styles.headerView, { borderBottomWidth: this.state.showActivity ? .5 : 0, marginHorizontal: 5 }]}>
                      <TouchableWithoutFeedback onPress={() => this.setState({ showActivity: !this.state.showActivity })}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 10, paddingBottom: 10 }}>
                          <View style={[styles.summaryRowView, { flex: 1 }]}>

                            <View >
                              <Image style={{ marginTop: 4 }} source={require('../images/analysisExportIcons/downArrow.png')} />
                            </View>

                            <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left", marginLeft: 10 }]}>
                              Activities
                      </Text>

                          </View>
                          {/* <Text style={styles.ProjectHeaderText}>
                      {this.state.TabPress ? "Projects" : "Type"}
                    </Text> */}
                          <Text style={styles.ProjectHeaderTime}>

                            40h 40m

                        </Text>
                        </View>
                      </TouchableWithoutFeedback>
                    </View>
                    {
                      this.state.showActivity ? this.createFlatList("activities") : null
                    }
                    {
                      this.state.showActivity ?
                        <View style={[styles.viewMoreView, { }]}>
                          <TouchableOpacity onPress={() => alert()}>
                            <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "right", marginRight: 20 }]}>
                              View more..
                            </Text>
                          </TouchableOpacity>
                        </View>
                        : null
                    }


                  </View>
                  : null}


                {/* Bar Graph View */}
                {this.state.TabPress ?
                  <View
                    style={{ backgroundColor: 'transparent', marginTop: 10, paddingBottom: 10, paddingRight: 15, borderWidth: 0, borderColor: "#E5E5E588", borderRadius: 6, marginBottom: 80 }}
                  >
                    <View style={[styles.headerView, { borderBottomWidth: this.state.showBarGraph ? .5 : 0, marginHorizontal: 5 }]}>
                      <TouchableWithoutFeedback onPress={() => this.setState({ showBarGraph: !this.state.showBarGraph })}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 10, paddingBottom: 10 }}>
                          <View style={[styles.summaryRowView, { flex: 1 }]}>

                            <View >
                              <Image style={{ marginTop: 4 }} source={require('../images/analysisExportIcons/downArrow.png')} />
                            </View>

                            <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left", marginLeft: 10 }]}>
                              Avg. Daily Time (Hrs)
                            </Text>

                          </View>
                        </View>
                      </TouchableWithoutFeedback>
                    </View>


                    {this.state.showBarGraph ?
                      <BarGraph />
                      : null}







                  </View>
                  : null}
              </View>
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: '#21459E'
  },
  container: { flex: 1, backgroundColor: '#F5FCFF', },
  mainContainer: { flex: 1, justifyContent: "center", alignItems: 'center', },
  projectListContainer: { margin: 10, width: width, },
  ProjectHeaderText: { fontSize: fontSizes.font16, color: "#2A56C6", textAlign: 'center' },
  ProjectHeaderTime: { fontSize: fontSizes.font14, color: "#FF9800", textAlign: 'center' },
  headerView: { padding: 10, borderBottomColor: '#E5E5E5', marginHorizontal: 10 },
  listView: { borderBottomWidth: .5, padding: 10, paddingTop: 5, paddingBottom: 5, borderBottomColor: '#E5E5E5', marginHorizontal: 10 },
  rowView: { flexDirection: "row", justifyContent: "space-between", padding: 5 },
  summaryColumnView: { paddingBottom: 5, padding: 5, borderBottomColor: "#E5E5E5" },
  summaryRowView: { flexDirection: "row", alignItems: "center", },
  projectColumnView: { flex: 1.5, justifyContent: "center", },
  //TimerContainer: { height: 100, width: width - 20, borderRadius: 6, borderWidth: .5, borderColor: "white", shadowOffset: { width: 0, height: 3, }, shadowColor: '#000', shadowOpacity: 0.8, shadowRadius: 5 }
  summaryContainer: { width: width - 20, borderRadius: 6, borderWidth: 0, borderColor: "#E5E5E588", padding: 10, marginTop: 10, paddingBottom: 10, paddingTop: 5, paddingRight: 15, paddingLeft: 12 },
  dropdownRowView: { flexDirection: "row", width: width - 20, marginHorizontal: 10 },
  leftDropdownStyle: { flex: 1, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, flexDirection: "row", justifyContent: "space-between", padding: 10, marginRight: 10 },
  rightDropdownStyle: { flex: 1, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, flexDirection: "row", justifyContent: "space-between", padding: 10, marginLeft: 10, marginRight: 20 },
  totalnotChangeText: { flex: 1, textAlign: "left", marginLeft: 25, fontSize: fontSizes.font10, color: "#757575", margin: 5 },
  viewMoreView: { width: width - 20, marginTop: 10, }


});
