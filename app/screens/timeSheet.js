
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text, SafeAreaView, Keyboard, TouchableWithoutFeedback,
  View, Image, ScrollView, TouchableOpacity, FlatList
} from 'react-native';



import CardView from 'react-native-cardview'
import Header from '../components/commonHeader'
import FloatingButton from '../components/floatingButton'
import LeaveModal from '../components/leaveModal'
import PickerDropdownModal from '../components/pickerDropdownModal'

import TimeLeaveTab from '../components/timesheetLeaveTab'
import { height, width, fontSizes } from '../utils/utils'
import { dateConverter } from '../components/dateConverter'
var interval
var keyboardOpen = false

const data = [
  1, 2, 3, 4, 5, 6, 7, 8
]
export default class timeSheet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      second: 0,
      minute: 0,
      TabPress: true,
      projectList: [],
      modalVisible: false,
      datePickerVisible: false, leaveType: 'Leave Type',
      leaveList: [], totalProjectHours: 0, totalLeaveDays: 0, totalDays: 0,
      returnCallBack: { value: false, callBack: () => { } },
      startDate: '', endDate: '', datePickerType: '', minDate: '', startDateToShowOnMOdal: '', endDateToShowOnModal: '',
      pickerDropdownModalVisible: false,
      selectProject: "Select Project",
      week: "Week",
      pickerDropdownModalTitleText: '',
      pickerDropdownModalListData: [],
      leaveTypeOnLeaveModal: 'Leave Type',
      leaveTypeList: []
    }
  }
  static navigationOptions = {
    drawerLabel: 'TimeSheet',
    drawerIcon: ({ tintColor }) => (
      <Image source={require('../images/drawerIcons/timesheetIcon.png')} />
    ),
  };
  componentWillMount() {
    var projectArr = [
      { "projectName": "ProjectXYZ", 'Activities': 2, 'progress': 80, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'Activities': 5, 'progress': 40, 'time': "7h 20m" },
      { "projectName": "ProjectXYZ", 'Activities': 6, 'progress': 60, 'time': "4h 20m" },
      { "projectName": "ProjectXYZ", 'Activities': 2, 'progress': 10, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'Activities': 2, 'progress': 80, 'time': "10h 20m" },
      { "projectName": "ProjectXYZ", 'Activities': 5, 'progress': 40, 'time': "7h 20m" },
      { "projectName": "ProjectXYZ", 'Activities': 6, 'progress': 60, 'time': "4h 20m" },
      { "projectName": "ProjectXYZ", 'Activities': 2, 'progress': 10, 'time': "10h 20m" },


    ]
    var leaveArr = [
      { "date": "16 May", 'type': "Sick Leave", 'status': "Approved", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
      { "date": "16 - 20 May", 'type': "Planned Leave", 'status': "Pending", 'totalDays': 4, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
      { "date": "14 May", 'type': "Sick Leave", 'status': "Not Approved", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
      { "date": "17 May", 'type': "Sick Leave", 'status': "Rejected", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
      { "date": "19 May", 'type': "Sick Leave", 'status': "Approved", 'totalDays': 1, "reason": "" },
      { "date": "23 May", 'type': "Sick Leave", 'status': "Approved", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
      { "date": "14 May", 'type': "Sick Leave", 'status': "Not Approved", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
      { "date": "17 May", 'type': "Sick Leave", 'status': "Rejected", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },
      { "date": "19 May", 'type': "Sick Leave", 'status': "Approved", 'totalDays': 1, "reason": "" },
      { "date": "23 May", 'type': "Sick Leave", 'status': "Approved", 'totalDays': 1, "reason": "These are the commen adsjhas adhkjhsad asdhbkjsa asdhas" },

    ]



    // var weekDaysArray = []
    // var date = new Date();
    // weekDaysArray.push()
    // date.setDate(date.getDate() + 27);
    var dateArr = ["7-13 May", "14-20 May", "21-28 May"]
    // var date=dateConverter(new Date())
    // alert(date)
    var days = 0
    var leaveTypeArr = []
    for (var i = 0; i < leaveArr.length; i++) {
      days = days + leaveArr[i].totalDays
      leaveTypeArr.push(leaveArr[i].type)
    }
    this.setState({ projectList: projectArr, leaveList: leaveArr, totalLeaveDays: days, weekArray: dateArr, leaveTypeList: leaveTypeArr })
    this.setState({ returnCallBack: { value: false, callBack: this.callBackCalledOnActivityAdd } })
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardOpen);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardClose);
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardOpen() {
    keyboardOpen = true
    //this.setState({ keyboardOpen: true })
  }

  keyboardClose() {
    keyboardOpen = false
    //this.setState({ keyboardOpen: false })
  }
  onTabPress() {
    this.setState({ TabPress: !this.state.TabPress })
  }
  navigateToLeaveDescription(item) {
    this.props.navigation.navigate('LeaveDescription', { data: item })
  }
  navigateToProjectBreakdown(item) {
    this.props.navigation.navigate('ProjectBreakdown', { data: item })
  }
  createList(item, index) {
    var progress = item.progress + "%"
    var left = (100 - item.progress) + "%"
    return (
      <TouchableWithoutFeedback onPress={() => this.state.TabPress ? this.navigateToProjectBreakdown(item) : this.navigateToLeaveDescription(item)}
        key={index + "row"} style={styles.listView}>
        <View style={styles.listView}>
          <View style={styles.rowView}>
            <View style={styles.projectColumnView}>
              <Text style={{ fontSize: fontSizes.font14, }}>
                {this.state.TabPress ? item.projectName : item.date}
              </Text>
              <Text style={{ fontSize: fontSizes.font14, color: "#BBBBBB", paddingTop: 5 }}>
                {this.state.TabPress ? item.Activities + " Activities" : item.type}
                {this.state.TabPress ? null :
                  <Text> | </Text>
                }
                {this.state.TabPress ? null :
                  <Text style={{ fontSize: fontSizes.font14, color: "green", paddingTop: 5 }}> {item.status}</Text>
                }
              </Text>
            </View>
            {this.state.TabPress ? <View style={[styles.projectColumnView, { flex: 2, marginHorizontal: 5, flexDirection: "row", alignItems: "center" }]}>
              <View style={{ width: progress, backgroundColor: "#2A56C6", height: 2 }} />
              <View style={{ width: left, backgroundColor: "#D8D8D8", height: 2 }} />
            </View> : null}
            <View style={[styles.projectColumnView, { flex: this.state.TabPress ? 1.5 : 0.5, flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }]}>

              <Text style={{ fontSize: fontSizes.font16, color: "#6C6C6C", marginRight: 10 }}>
                {this.state.TabPress ? item.time : item.totalDays > 1 ? item.totalDays + " Days" : item.totalDays + " Day"}
              </Text>
              <Image source={require('../images/rightArrow.png')} />

            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }

  // timesheet_LeavesTab() {
  //   return (
  //     <View style={styles.tabView}>
  //       {/* first tab */}
  //       <TouchableOpacity onPress={() => this.onTabPress()}
  //         style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.TabPress ? 2 : 0 }]} >
  //         <Text style={styles.tabText}>
  //           Timesheet
  //             </Text>
  //       </TouchableOpacity>
  //       {/* second tab */}
  //       <TouchableOpacity onPress={() => this.onTabPress()}
  //         style={[styles.tabStyle, { borderBottomColor: "white", borderBottomWidth: this.state.TabPress ? 0 : 2 }]}   >
  //         <Text style={styles.tabText}>
  //           Leaves
  //                           </Text>
  //       </TouchableOpacity>
  //       {/* third tab */}
  //       <View style={styles.tabStyle} >
  //       </View>
  //     </View>
  //   )
  // }
  onWeekTabPress() {
    this.setState({ weekArray: ["14-20 May", "20-28 May", "28 May-3 Jun"] })
  }
  weekTabs() {
    return (
      <View style={styles.weekTabView}>
        {/* first tab */}
        <TouchableOpacity onPress={() => this.onWeekTabPress()}
          style={[styles.tabStyle]} >
          <Text style={[styles.tabText, { color: "#2A56C6" }]}>
            {this.state.weekArray[0]}
          </Text>
        </TouchableOpacity>
        {/* second tab */}
        <TouchableOpacity onPress={() => this.onWeekTabPress()}
          style={[styles.tabStyle]}   >
          <Text style={[styles.tabText, { color: "#000" }]}>
            {this.state.weekArray[1]}
          </Text>
        </TouchableOpacity>
        {/* third tab */}
        <TouchableOpacity onPress={() => this.onWeekTabPress()}
          style={[styles.tabStyle]}   >
          <Text style={[styles.tabText, { color: "#2A56C6" }]}>
            {this.state.weekArray[2]}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
  onFloatingButtonPressed() {
    !this.state.TabPress ?
      this.setState({ modalVisible: true })
      : this.props.navigation.navigate("CreateTimesheet", { data: { returnCallBack: this.state.returnCallBack } })
  }
  callBackCalledOnActivityAdd = (data) => {
    this.state.projectList.push({ "projectName": data.projectName, 'Activities': data.activity, 'progress': 0, 'time': data.time })
    this.setState({ projectList: this.state.projectList })

    //this callback is called when we click on + floating icon and add a new activity that is added to this screen flatlist
  }
  getDateFromModal(date) {
    console.log("getDateFromModal" + date)
    if (this.state.datePickerType == 'start') {
      this.setState({ startDate: date, datePickerType: '', minDate: date, datePickerVisible: false, })
      var d = dateConverter(date)
      setTimeout(() => {
        this.setState({ startDateToShowOnMOdal: d })
      })
    } if (this.state.datePickerType == 'end') {
      this.setState({ endDate: date, datePickerType: '', datePickerVisible: false })
      var d = dateConverter(date)
      setTimeout(() => {
        this.setState({ endDateToShowOnModal: d })
      })
    }

    this.totalDaysCalculate()
  }
  totalDaysCalculate() {
    if (this.state.endDate != '') {
      var date1 = new Date(this.state.startDate);
      var date2 = new Date(this.state.endDate);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      this.setState({ totalDays: diffDays + 1 })
      //alert(this.state.endDate - this.state.totalDays)
    }

  }
  startDateVisibleFunction() {
    this.setState({ datePickerVisible: true, datePickerType: 'start', minDate: '' })
  }
  endDateVisibleFunction() {
    if (this.state.startDate == '') {
      alert("Please select start date first")
    } else {
      this.setState({ datePickerVisible: true, datePickerType: 'end' })
    }

  }
  saveButtonClicked() {
    this.setState({ modalVisible: false })
    //alert(this.state.reasonTextInputData)
  }
  modalClose() {
    if (keyboardOpen) {
      Keyboard.dismiss()
    } else {
      this.setState({ modalVisible: false })
    }

  }
  onTimerStart() {
    interval = setInterval(() => {
      //seconds++
      //console.log(Math.floor(this.state.second / 60))
      if (this.state.minute % 5 == 0) {
        this.setState({ second: this.state.second + 2 })
      } 
      else if (this.state.minute % 15 == 0) {
        this.setState({ second: this.state.second + 2 })
      }
      else if (this.state.minute % 20 == 0) {
        this.setState({ second: this.state.second + 2 })
      }
      else if (this.state.minute % 25 == 0) {
        this.setState({ second: this.state.second + 2 })
      } else {
        this.setState({ second: this.state.second + 1 })
      }


      if ((this.state.second / 60) > 59) {
        var min = this.state.second / 60
        while (min >= 60) {
          min = min - 60
        }
        this.setState({ minute: min })
      } else {
        this.setState({ minute: (this.state.second / 60) })
      }
    }, 1000)
  }

  onTimerStop() {
    clearInterval(interval);
    this.setState({ second: 0 })
  }
  onProjectSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, PickerDropdownModalType: "Select Project", pickerDropdownModalListData: this.state.projectList })
  }
  pickerDropdownModalRowSelected(item) {
    if (this.state.PickerDropdownModalType == "Select Project") {
      this.setState({ selectProject: item.projectName })

    } else if (this.state.PickerDropdownModalType == 'Select Leave Type') {
      this.setState({ leaveTypeOnLeaveModal: item.type })
    }
    else {
      this.setState({ week: item })

    }
    this.setState({ pickerDropdownModalVisible: false })
  }
  onWeekSelectDropdown() {
    this.setState({ pickerDropdownModalVisible: true, PickerDropdownModalType: "Week", pickerDropdownModalListData: data })
  }
  onLeaveModalselectLeaveTypeClicked(index, value) {
    this.setState({ leaveTypeOnLeaveModal: value })
  }
  render() {
    return (
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.container}>
          <Header HeaderLeftText={this.state.week}
            HeaderRightText="Analysis"
            toggleDrawer={() => this.props.navigation.openDrawer()}
            headerLeftIcon={require('../images/headerIcons/downArrow.png')}
            headerRightIcon={require('../images/headerIcons/analysis.png')}
            headerLeftIconOnPress={() => this.onWeekSelectDropdown()} />
          <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatPlusIcon.png')} />


          <LeaveModal
            modalVisible={this.state.modalVisible}
            datePickerVisible={this.state.datePickerVisible}
            datePickerVisibleFunction={() => this.setState({ datePickerVisible: true })}
            startDatePickerVisibleFunction={() => this.startDateVisibleFunction()}
            endDatePickerVisibleFunction={() => this.endDateVisibleFunction()}
            dateSelected={(date) => this.getDateFromModal(date)}
            startDate={this.state.startDateToShowOnMOdal}
            endDate={this.state.endDateToShowOnModal}
            totalDays={this.state.totalDays}
            datePickerCloseFunction={() => this.setState({ datePickerVisible: false })}
            minDate={new Date(this.state.startDateToShowOnMOdal)}
            saveButtonClicked={() => this.saveButtonClicked()}
            reasonTextInputData={(text) => this.setState({ reasonTextInputData: text })}
            modalClose={() => this.modalClose()}
            TitleText="Add Leave" TypePlaceHolder={this.state.leaveTypeOnLeaveModal}
            leavesOptionArrayOnLeaveModal={this.state.leaveTypeList}
            selectLeaveTypeClicked={(index, value) => this.onLeaveModalselectLeaveTypeClicked(index, value)}
          />

          <PickerDropdownModal
            pickerDropdownModalVisible={this.state.pickerDropdownModalVisible}
            pickerDropdownModalClose={() => this.setState({ pickerDropdownModalVisible: false })}
            titleText={this.state.PickerDropdownModalType}
            pickerDropdownModalListData={this.state.pickerDropdownModalListData}
            pickerDropdownModalRow={({ item, index }) =>
              <TouchableWithoutFeedback onPress={() => this.pickerDropdownModalRowSelected(item)}>
                <View style={{ padding: 15, paddingLeft: 25, alignItems: "flex-start", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1 }}>
                  <Text style={{ fontSize: fontSizes.font18, color: "#333333" }}>
                    {item.projectName ? item.projectName : item.type ? item.type : item}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            }
          />




          <View style={styles.mainContainer}>
            {/* {this.timesheet_LeavesTab()} */}
            <TimeLeaveTab onTabPress={() => this.onTabPress()} tabPress={this.state.TabPress} />
            {this.state.weekArray ? this.weekTabs() : null}
            <ScrollView keyboardShouldPersistTaps="always">
              <View style={styles.projectListContainer}>
                {this.state.TabPress ?
                  // <CardView
                  //   style={{ backgroundColor: 'transparent' }}
                  //   cardElevation={2}

                  //   cardMaxElevation={2}
                  //   cornerRadius={2}>
                  <View style={styles.TimerContainer}>
                    <View style={styles.rowView}>
                      <Text style={[styles.ProjectHeaderText, { flex: 1, textAlign: "left" }]}>
                        Quick Log
                                    </Text>
                      <TouchableOpacity onPress={() => this.onProjectSelectDropdown()}
                        style={{ flex: 1, borderBottomColor: "#D8D8D8", borderBottomWidth: 1, flexDirection: "row", justifyContent: "space-between" }}>
                        <Text style={[styles.ProjectHeaderText, { color: 'black' }]}>
                          {this.state.selectProject}
                        </Text>
                        <Image style={{ marginTop: 4 }} source={require('../images/downArrow.png')} />
                      </TouchableOpacity>
                    </View>
                    <View style={[styles.rowView, { paddingTop: 10, marginTop: 10 }]}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <TouchableOpacity onPress={() => this.onTimerStart()}
                          style={{ flex: 1, alignItems: "center" }}>
                          <Image source={require('../images/timesheetIcon/playIcon.png')} />
                          <Text style={[styles.ProjectHeaderText, { color: 'black', marginTop: 5 }]}>
                            Start
                                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.onTimerStop()}
                          style={{ flex: 1, alignItems: "center", marginTop: 2 }}>
                          <Image source={require('../images/timesheetIcon/greyStopIcon.png')} />
                          <Text style={[styles.ProjectHeaderText, { color: 'black', marginTop: 5 }]}>
                            Stop
                                            </Text>
                        </TouchableOpacity>
                      </View>


                      <View style={{ flex: 1, alignItems: "flex-end", justifyContent: "center" }}>
                        <Text style={[styles.ProjectHeaderText, { color: 'black', fontSize: 32 }]}>
                          {/* {(Math.floor(this.state.second / 3600) > 9 ? Math.floor(this.state.second / 3600) : "0" + Math.floor(this.state.second / 3600)) + ":" +
                            (Math.floor(this.state.second / 60) > 9 ? Math.floor(this.state.second / 60) : "0" + Math.floor(this.state.second / 60)) +
                            ':' +
                            ('0' + this.state.second % 60).slice(-2)} */}
                          {(Math.floor(this.state.second / 3600) > 9 ? Math.floor(this.state.second / 3600) : "0" + Math.floor(this.state.second / 3600)) + ":" +
                            (Math.floor(this.state.minute) > 9 ? Math.floor(this.state.minute) : "0" + Math.floor(this.state.minute)) +
                            ':' +
                            ('0' + this.state.second % 60).slice(-2)}
                          {/* {(Math.floor(this.state.second / 3600) > 9 ? Math.floor(this.state.second / 3600) : "0" + Math.floor(this.state.second / 3600)) + ":" +
                            (('0' + this.state.second % 3600).slice(-2) > 9 ? ('0' + this.state.second % 3600).slice(-2) :  ('0' + this.state.second % 3600).slice(-2)) +
                            ':' +
                            ('0' + this.state.second % 60).slice(-2)} */}
                        </Text>
                      </View>
                    </View>
                  </View>
                  // </CardView>
                  : null}
                <View style={styles.headerView}>
                  <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                    <Text style={styles.ProjectHeaderText}>
                      {this.state.TabPress ? "Projects" : "On"}
                    </Text>
                    <Text style={styles.ProjectHeaderTime}>

                      {this.state.TabPress ? "40h 40m" : this.state.totalLeaveDays > 1 ? this.state.totalLeaveDays + " Days" : this.state.totalLeaveDays + " Day"}

                    </Text>
                  </View>
                </View>
                <FlatList
                  data={this.state.TabPress ? this.state.projectList : this.state.leaveList}
                  renderItem={({ item, index }) => this.createList(item, index)}
                  keyExtractor={key => key.index}
                  extraData={this.state}
                />

              </View>
            </ScrollView>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#F5FCFF', justifyContent: "flex-start", },
  safeArea: {
    flex: 1,
    backgroundColor: '#21459E'
  },
  tabText: { fontSize: fontSizes.font14, color: "white", textAlign: 'center', fontWeight: "bold" },
  instructions: { textAlign: 'center', color: '#333333', marginBottom: 5, },
  mainContainer: { flex: 1, justifyContent: "center", alignItems: 'center', },
  tabView: { height: 40, width: width, flexDirection: 'row', backgroundColor: "#2A56C6" },
  weekTabView: { height: 20, width: width, flexDirection: 'row', backgroundColor: "#979797" },
  tabStyle: { flex: 1, alignItems: "center", justifyContent: "center" },
  projectListContainer: { margin: 10, width: width - 20, },
  ProjectHeaderText: { fontSize: fontSizes.font16, color: "#2A56C6", textAlign: 'center' },
  ProjectHeaderTime: { fontSize: fontSizes.font14, color: "#FF9800", textAlign: 'center' },
  headerView: { borderBottomWidth: .5, padding: 10, borderBottomColor: '#979797' },
  listView: { borderBottomWidth: .5, padding: 10, paddingTop: 5, paddingBottom: 5, borderBottomColor: '#BBBBBB' },
  rowView: { flexDirection: "row", justifyContent: "space-between", padding: 5 },
  projectColumnView: { flex: 1.5, justifyContent: "center", },
  //TimerContainer: { height: 100, width: width - 20, borderRadius: 6, borderWidth: .5, borderColor: "white", shadowOffset: { width: 0, height: 3, }, shadowColor: '#000', shadowOpacity: 0.8, shadowRadius: 5 }
  TimerContainer: { width: width - 20, borderRadius: 6, borderWidth: 0, paddingRight: 10, paddingBottom: 10, borderColor: "#E5E5E588", borderRadius: 5, padding: 5 }

});
