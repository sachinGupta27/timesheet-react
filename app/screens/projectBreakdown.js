



/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet, SafeAreaView,
    Text, ScrollView, FlatList,
    View, Image, TouchableOpacity, TouchableWithoutFeedback
} from 'react-native';
import Header from '../components/commonHeader'
import FloatingButton from '../components/floatingButton'
import PickerDropdownModal from '../components/pickerDropdownModal'
import { height, width, fontSizes } from '../utils/utils'

const data = [
    1, 2, 3, 4, 5, 6, 7, 8
]
export default class projectBreakdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [], weekArray: [], ActivityList: [], returnCallBack: { value: false, callBack: () => { } },
            pickerDropdownModalVisible: false,
            pickerDropdownModalTitleText: "Week",
            week: "Week"
        }
    }
    componentWillMount() {
        //alert(JSON.stringify(this.props.navigation.state.params.data))
        this.setState({ data: this.props.navigation.state.params.data })
        // setTimeout(() => {
        //     alert(JSON.stringify(this.state.data))
        // }, 500)
        var dateArr = ["7-13 May", "14-20 May", "21-28 May"]
        var activityArr = [
            { "activityName": "Development", "Billable": true, "time": "2h 3m" },
            { "activityName": "Meeting", "Billable": false, "time": "3h 3m" },
            { "activityName": "Development", "Billable": true, "time": "4h 3m" },
            { "activityName": "Development", "Billable": true, "time": "5h 3m" },
            { "activityName": "Development", "Billable": true, "time": "2h 3m" },
            { "activityName": "Meeting", "Billable": false, "time": "3h 3m" },
            { "activityName": "Development", "Billable": true, "time": "4h 3m" },
            { "activityName": "Development", "Billable": true, "time": "5h 3m" },
            { "activityName": "Development", "Billable": true, "time": "2h 3m" },
            { "activityName": "Meeting", "Billable": false, "time": "3h 3m" },
            { "activityName": "Development", "Billable": true, "time": "4h 3m" },
            { "activityName": "Development", "Billable": true, "time": "5h 3m" },
        ]
        this.setState({ weekArray: dateArr, ActivityList: activityArr })
        this.setState({ returnCallBack: { value: false, callBack: this.callBackCalledOnActivityAdd } })
    }
    weekTabs() {
        return (
            <View style={styles.weekTabView}>
                {/* first tab */}
                <TouchableOpacity onPress={() => this.onWeekTabPress()}
                    style={[styles.tabStyle]} >
                    <Text style={[styles.tabText, { color: "#2A56C6" }]}>
                        {this.state.weekArray[0]}
                    </Text>
                </TouchableOpacity>
                {/* second tab */}
                <TouchableOpacity onPress={() => this.onWeekTabPress()}
                    style={[styles.tabStyle]}   >
                    <Text style={[styles.tabText, { color: "#000" }]}>
                        {this.state.weekArray[1]}
                    </Text>
                </TouchableOpacity>
                {/* third tab */}
                <TouchableOpacity onPress={() => this.onWeekTabPress()}
                    style={[styles.tabStyle]}   >
                    <Text style={[styles.tabText, { color: "#2A56C6" }]}>
                        {this.state.weekArray[2]}
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
    callBackCalledOnActivityAdd = (data) => {
        //alert(JSON.stringify(data))
        this.state.ActivityList.push({ "activityName": data.activity, "Billable": data.billable, "time": data.time })
        this.setState({ ActivityList: this.state.ActivityList })

        //this callback is called when we click on + floating icon and add a new activity that is added to this screen flatlist
    }
    onWeekTabPress() {
        this.setState({ weekArray: ["14-20 May", "20-28 May", "28 May-3 Jun"] })
    }

    createList(item, index) {
        return (
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate("ProjectDevelopment", { data: item, projectName: this.state.data.projectName })}
                key={index + "row"} >
                <View style={styles.listView}>
                    <View style={styles.listRowView}>
                        <View style={{ flex: 1, alignItems: "center", justifyContent: "center", }}>
                            <View style={{ height: 20, width: 20, borderRadius: 10, backgroundColor: "blue" }}></View>
                        </View>
                        <View style={[styles.projectColumnView,]}>
                            <Text style={{ fontSize: fontSizes.font14, }}>
                                {item.activityName}
                            </Text>
                            <Text style={{ fontSize: fontSizes.font12, color: "#BBBBBB", paddingTop: 5 }}>
                                {item.Billable ? "Billable" : "Non Billable"}
                            </Text>
                        </View>

                        <View style={[styles.projectColumnView, { flex: 2, flexDirection: "row", alignItems: "center", justifyContent: "flex-end" }]}>

                            <Text style={{ fontSize: fontSizes.font16, color: "#6C6C6C", marginRight: 10 }}>
                                {item.time}
                            </Text>
                            <Image source={require('../images/rightArrow.png')} />

                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        )
    }
    onFloatingButtonPressed() {
        this.props.navigation.navigate("CreateTimesheet", { data: { projectName: this.state.data.projectName, returnCallBack: this.state.returnCallBack } })
    }
    pickerDropdownModalRowSelected(item) {
        this.setState({ week: item, pickerDropdownModalVisible: false })
    }
    render() {

        return (
            <SafeAreaView style={styles.safeArea}>
                <View style={styles.container}>
                    <Header
                        HeaderLeftText={this.state.data.projectName}
                        headerType='back'
                        HeaderRightText={this.state.week}

                        toggleDrawer={() => this.props.navigation.goBack()}
                        //headerLeftIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIcon={require('../images/headerIcons/downArrow.png')}
                        headerRightIconOnPress={() => this.setState({ pickerDropdownModalVisible: true })}
                    />
                    <PickerDropdownModal
                        pickerDropdownModalVisible={this.state.pickerDropdownModalVisible}
                        pickerDropdownModalClose={() => this.setState({ pickerDropdownModalVisible: false })}
                        titleText={this.state.pickerDropdownModalTitleText}
                        pickerDropdownModalListData={data}
                        pickerDropdownModalRow={({ item, index }) =>
                            <TouchableWithoutFeedback onPress={() => this.pickerDropdownModalRowSelected(item)}>
                                <View style={{ padding: 15, paddingLeft: 25, alignItems: "flex-start", justifyContent: "flex-start", borderBottomColor: "#E5E5E5", borderBottomWidth: 1 }}>
                                    <Text style={{ fontSize: fontSizes.font18, color: "#333333" }}>
                                        {item}
                                    </Text>
                                </View>
                            </TouchableWithoutFeedback>
                        }
                    />
                    <FloatingButton onButtonPressed={() => this.onFloatingButtonPressed()} icon={require('../images/floatPlusIcon.png')} />
                    <View style={styles.mainContainer}>
                        {this.weekTabs()}
                        <View style={styles.rowView}>
                            <Text style={styles.headerTextStyle}>Activity</Text>
                            <Text style={[styles.headerTextStyle, { fontSize: fontSizes.font18, color: "#FF9800" }]}>{this.state.data.time}</Text>
                        </View>

                        <FlatList
                            data={this.state.ActivityList}
                            renderItem={({ item, index }) => this.createList(item, index)}
                            keyExtractor={key => key.index}
                        />


                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: '#F5FCFF'
    },
    safeArea: {
        flex: 1,
        backgroundColor: '#21459E'
    },
    weekTabView: { height: 20, width: width, flexDirection: 'row', backgroundColor: "#979797" },
    tabStyle: { flex: 1, alignItems: "center", justifyContent: "center" },
    tabText: { fontSize: fontSizes.font14, color: "white", textAlign: 'center', fontWeight: "bold" },
    mainContainer: { flex: 1, borderBottomColor: "#E5E5E5", borderBottomWidth: 1 },
    rowView: { flexDirection: "row", justifyContent: "space-between", paddingBottom: 10, padding: 3, borderBottomColor: "#E5E5E5", borderBottomWidth: 1, margin: 15, marginBottom: 10, alignItems: "center" },
    headerTextStyle: { fontSize: fontSizes.font16, color: "#2A56C6", textAlign: 'left' },
    listView: { borderBottomWidth: .5, marginHorizontal: 5, marginTop: 0, paddingBottom: 5, borderBottomColor: '#E5E5E5', marginRight: 10 },
    listRowView: { flexDirection: "row", justifyContent: "space-between", padding: 5 },
    projectColumnView: { flex: 4, justifyContent: "center", },
});
